# TravelInsurance

Customer Journey for travelling insurance

**Pre-requisites**
- Java 1.8
- Eclipse IDE

**Framework**
- Springboot
- JSP

**Steps**
1. open the terminal, execute "mvnw spring-boot:run"
2. browse http://localhost:8080/ to access the web app.
3. browse http://localhost:8080/h2-console to access the h2 DB.
    - JDBC URL = jdbc:h2:~/travel_insurancedb;

