DROP TABLE IF EXISTS price;

CREATE TABLE price (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  plan varchar(1) NOT NULL,
  coverage INT NOT NULL,
  area INT NOT NULL,
  price int NOT NULL
);

DROP TABLE IF EXISTS policy;

CREATE TABLE policy (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  coverage_type INT NOT NULL,
  area INT NOT NULL,
  start_date TIMESTAMP NOT NULL,
  end_date TIMESTAMP NOT NULL,
  plan varchar(1),
  customer_id int
);

DROP TABLE IF EXISTS customer;

CREATE TABLE customer (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(200) NOT NULL,
  nric VARCHAR(20) NOT NULL,
  dob TIMESTAMP,
  gender VARCHAR(10),
  email VARCHAR(200) NOT NULL,
  mobile_no VARCHAR(20),
  address1 VARCHAR(200),
  address2 VARCHAR(200),
  postcode VARCHAR(10)
);
