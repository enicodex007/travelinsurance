<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8">
        <title>Self Care Travel Insurance</title>
        <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
        <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
        <script src="webjars/bootstrap-datepicker/1.0.1/js/bootstrap-datepicker.js"></script>
        <link th:rel="stylesheet" th:href="@{webjars/bootstrap/3.3.6/css/bootstrap.min.css}" />    
        <link th:rel="stylesheet" th:href="@{webjars/bootstrap-datepicker/1.0.1/css/datepicker.css}" />
    </head>
    <body>
    	<div class="container" th:if="${journey ne null}">
    		  <h2 class="mt-5">Coverage : <strong th:text="${journey.coverageType}" ></strong></h2>
    		  <h2 class="mt-5">Area : <strong th:text="${journey.area}" ></strong></h2>
    		  <h2 class="mt-5">Start Date : <strong th:text="${journey.startDate}" ></strong></h2>
    		  <h2 class="mt-5">End Date : <strong th:text="${journey.endDate}" ></strong></h2>
    	</div>
    	
    	<form action="#" method="post" th:action="@{/savePlan}" th:object="${planForm}" >
    		<div class="container">
    		  <input type="hidden" th:field="*{policyId}"/>
			  <fieldset>
				<legend>Plan</legend>
				<div th:each="price : ${planForm.planPrices}">
					<div>
						<input type="radio" th:field="*{plan}" th:value="${price.plan}">
						<label th:for="${#ids.prev('plan')}" th:text="${'Plan '+price.plan +'-' + 'RM' + price.price}">th:text="${'Plan'+price.plan + '-' + 'RM' + price.price}"</label>
					</div>
				</div>
				</fieldset>
				<button class="btn btn-primary" type="submit">Next</button>    
			</div>
    	</form>
	</body>
</html>