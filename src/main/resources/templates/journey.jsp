<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8">
        <title>Self Care Travel Insurance</title>
        <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
        <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
        <script src="webjars/bootstrap-datepicker/1.0.1/js/bootstrap-datepicker.js"></script>
        <link th:rel="stylesheet" th:href="@{webjars/bootstrap/3.3.6/css/bootstrap.min.css}" />    
        <link th:rel="stylesheet" th:href="@{webjars/bootstrap-datepicker/1.0.1/css/datepicker.css}" />
    </head>
    <body>
		<div class="container">
		    <form action="#" method="post" th:action="@{/saveJourney}" th:object="${journey}">
				<div class="form-group">
					<fieldset>
						<legend>Coverage</legend>
						<div th:each="coverageType : ${T(com.apd.travelinsurance.constant.CoverageType).values()}">
							<div>
								<input type="radio" th:field="*{coverageType}" th:value="${coverageType}">
								<label th:for="${#ids.prev('coverageType')}" th:text="${coverageType}">Coverage</label>
								<p class="alert alert-danger" th:if="${#fields.hasErrors('coverageType')}" th:errors="*{coverageType}" />
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Areas</legend>
						<div th:each="area : ${T(com.apd.travelinsurance.constant.Area).values()}">
							<div>
								<input type="radio" th:field="*{area}" th:value="${area}">
								<label th:for="${#ids.prev('area')}" th:text="${area}">Area</label>
								<p class="alert alert-danger" th:if="${#fields.hasErrors('area')}" th:errors="*{area}" />
							</div>
						</div>
					</fieldset>
					 
					 <fieldset>
					 	<legend>Your Journey time</legend>
					 	<label class="control-label">Start Date</label>
					 	<input type='text' class="form-control" th:field="*{startDate}" placeholder="dd/MM/yyyy" />
					 	<p class="alert alert-danger" th:if="${#fields.hasErrors('startDate')}" th:errors="*{startDate}" />
					 	&nbsp;
					 	<label class="control-label">End Date</label>
					 	<input type='text' class="form-control" th:field="*{endDate}" placeholder="dd/MM/yyyy" />
					 	<p class="alert alert-danger" th:if="${#fields.hasErrors('endDate')}" th:errors="*{endDate}" />
		                <!-- <div class='input-group date' id='datetimepicker1'>
		                	<input type='text' class="form-control" th:field="*{startDate}" placeholder="dd/MM/yyyy" />
		                   <span class="input-group-addon">
		                   	<span class="glyphicon glyphicon-calendar"></span>
		                   </span>
		                </div> -->
		                <!-- <label class="control-label">End Date</label>
		                <div class='input-group date' id='datetimepicker2'>
		                   <input type='text' class="form-control" th:field="*{endDate}" placeholder="dd/MM/yyyy" />
		                   <span class="input-group-addon">
		                   	<span class="glyphicon glyphicon-calendar"></span>
		                   </span>
		                </div> -->
					 </fieldset>
		        	<button class="btn btn-primary" type="submit">Next</button>        
				 </div>
				
		    </form>
		</div>
		
		
		<script>
		    $('.input-group.date').datepicker({
		        autoclose: true,
		        todayHighlight: true,
		        format: "mm/dd/yyyy"
		    });
		</script>
	</body>
</html>