<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8">
        <title>Self Care Travel Insurance</title>
        <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
        <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
        <script src="webjars/bootstrap-datepicker/1.0.1/js/bootstrap-datepicker.js"></script>
        <link th:rel="stylesheet" th:href="@{webjars/bootstrap/3.3.6/css/bootstrap.min.css}" />    
        <link th:rel="stylesheet" th:href="@{webjars/bootstrap-datepicker/1.0.1/css/datepicker.css}" />
    </head>
    <body>
    	<form action="#" method="post" th:action="@{/saveCustomer}" th:object="${summary}">
    		<div class="container">
			  
    		<h3 class="mt-5">Dear <strong th:text="${summary.customer.name}" ></strong>, You've successfully purchased the plan! </h2>
					 <h2 class="mt-5">Your plan info:</h2>
			 		<table class="table table-striped table-responsive-md">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td th:text="${'Plan: '+ summary.customer.plan}"></td>
                                <td th:text="${'Coverage: '+ summary.journey.coverageType}"></td>
                            </tr>
                            <tr>
                                <td colspan=2 th:text="${'Area: '+ summary.journey.area}"></td>
                            </tr>
                            <tr>
                                <td th:text="${'Start Date: '+ summary.journey.startDate}"></td>
                                <td th:text="${'End Date: '+ summary.journey.endDate}"></td>
                            </tr>
                        </tbody>
                    </table>
                    <h2 class="mt-5">Insured Information:</h2>
                    <table class="table table-striped table-responsive-md">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td th:text="${'Name: '+ summary.customer.name}"></td>
                                <td th:text="${'NRIC: '+ summary.customer.nric}"></td>
                            </tr>
                            <tr>
                                <td colspan="2" th:text="${'Gender: '+ summary.customer.gender}"></td>
                            </tr>
                            <tr>
                                <td th:text="${'Email: '+ summary.customer.email}"></td>
                                <td th:text="${'Mobile No: '+summary.customer.mobileNo}"></td>
                            </tr>
                            <tr>
                                <td colspan="2" th:text="${'Address 1: '+summary.customer.address1}"></td>
                            </tr>
                            <tr>
                                <td colspan="2" th:text="${'Address 2: '+summary.customer.address2}"></td>
                            </tr>
                            <tr>
                                <td colspan="2" th:text="${'Postcode: '+summary.customer.postcode}"></td>
                            </tr>
                            
                        </tbody>
                    </table>
			
			
			<a class="btn btn-success" href="/"><i class="fa fa-arrow-circle-o-left"></i>&nbsp;Back</a>    
			</div>
    	</form>
    	
	</body>
</html>