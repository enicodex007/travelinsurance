<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="UTF-8">
        <title>Self Care Travel Insurance</title>
        <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
        <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
        <script src="webjars/bootstrap-datepicker/1.0.1/js/bootstrap-datepicker.js"></script>
        <link th:rel="stylesheet" th:href="@{webjars/bootstrap/3.3.6/css/bootstrap.min.css}" />    
        <link th:rel="stylesheet" th:href="@{webjars/bootstrap-datepicker/1.0.1/css/datepicker.css}" />
    </head>
    <body>
    	<form action="#" method="post" th:action="@{/saveCustomer}" th:object="${customer}">
    		<div class="container">
			  <input type="hidden" th:field="*{policyId}"/>
			  <input type="hidden" th:field="*{plan}"/>
    		  <h2 class="mt-5">Your've selected plan : <strong th:text="${customer.plan}" ></strong></h2>
			  
			  <div class="form-group" th:classappend="${#fields.hasErrors('name')}? 'has-error':''">
			      <label for="name" class="control-label">Name</label> 
			      <input id="name" class="form-control" th:field="*{name}" />
			      <p class="error-message" th:each="error: ${#fields.errors('name')}" th:text="${error}">Validation error</p>
			  </div>
			  <div class="form-group" th:classappend="${#fields.hasErrors('nric')}? 'has-error':''">
			      <label for="nric" class="control-label">NRIC</label> 
			      <input id="nric" class="form-control" th:field="*{nric}" />
			      <p class="error-message" th:each="error: ${#fields.errors('nric')}" th:text="${error}">Validation error</p>
			  </div>
			  
			  <div class="form-group" th:classappend="${#fields.hasErrors('dob')}? 'has-error':''">
			      <label for="dob" class="control-label">DOB</label> 
			      <input id="dob" type="text" class="form-control" th:field="*{dob}" placeholder="dd/MM/yyyy"/>
			      <p class="error-message" th:each="error: ${#fields.errors('dob')}" th:text="${error}">Validation error</p>
			  </div>
			  
			  <div class="form-group" th:classappend="${#fields.hasErrors('gender')}? 'has-error':''">
			      <label for="gender" class="control-label">Gender</label> 
			      <input id="gender" class="form-control" th:field="*{gender}" placeholder="M/F"/>
			      <p class="error-message" th:each="error: ${#fields.errors('gender')}" th:text="${error}">Validation error</p>
			  </div>
			  
			   <div class="form-group" th:classappend="${#fields.hasErrors('email')}? 'has-error':''">
			      <label for="email" class="control-label">EMAIL</label> 
			      <input id="email" class="form-control" th:field="*{email}" />
			      <p class="error-message" th:each="error: ${#fields.errors('email')}" th:text="${error}">Validation error</p>
			  </div>
			  
			   <div class="form-group" th:classappend="${#fields.hasErrors('mobileNo')}? 'has-error':''">
			      <label for="mobileNo" class="control-label">Mobile No</label> 
			      <input id="mobileNo" class="form-control" th:field="*{mobileNo}" />
			      <p class="error-message" th:each="error: ${#fields.errors('mobileNo')}" th:text="${error}">Validation error</p>
			  </div>
			  
			  <div class="form-group">
			      <label for="address1" class="control-label">Address 1</label> 
			      <input id="address1" class="form-control" th:field="*{address1}" />
			       <br>
			      <label for="address2" class="control-label">Address 2</label> 
			      <input id="address2" class="form-control" th:field="*{address2}" />  
			  </div>
			  	
			  <div class="form-group" th:classappend="${#fields.hasErrors('postcode')}? 'has-error':''">
			      <label for="postcode" class="control-label">Postcode</label> 
			      <input id="postcode" class="form-control" th:field="*{postcode}" />
			      <p class="error-message" th:each="error: ${#fields.errors('postcode')}" th:text="${error}">Validation error</p>
			  </div>
			  
			  
				<button class="btn btn-primary" type="submit">Next</button>    
			</div>
    	</form>
    	
	</body>
</html>