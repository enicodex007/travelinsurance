package com.apd.travelinsurance.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apd.travelinsurance.entity.Price;
import com.apd.travelinsurance.repository.PriceRepository;

@Service
public class PriceService {

	@Autowired
	PriceRepository priceRepository;

	public List<Price> getAllPrice() {
		List<Price> prices = new ArrayList<>();
		priceRepository.findAll().forEach(price -> prices.add(price));
		return prices;
	}

	public Price getPriceById(int id) {
		return priceRepository.findById(id).get();
	}

	public void saveOrUpdate(Price price) {
		priceRepository.save(price);
	}

	public void delete(int id) {
		priceRepository.deleteById(id);
	}
}
