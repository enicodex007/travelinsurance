package com.apd.travelinsurance.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apd.travelinsurance.entity.Customer;
import com.apd.travelinsurance.entity.Policy;
import com.apd.travelinsurance.repository.CustomerRepository;
import com.apd.travelinsurance.repository.PolicyRepository;

@Service
public class CustomerService {

	@Autowired
	CustomerRepository customerRepository;

	public List<Customer> getAllCustomer() {
		List<Customer> policies = new ArrayList<>();
		customerRepository.findAll().forEach(customer -> policies.add(customer));
		return policies;
	}

	public Customer getCustomerById(int id) {
		return customerRepository.findById(id).get();
	}

	public void saveOrUpdate(Customer customer) {
		customerRepository.save(customer);
	}

	public void delete(int id) {
		customerRepository.deleteById(id);
	}
	
	public Customer findByEmail(String email) {
		return customerRepository.findByEmail(email);
	}
	
	
}
