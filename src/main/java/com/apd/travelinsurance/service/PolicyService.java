package com.apd.travelinsurance.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apd.travelinsurance.entity.Policy;
import com.apd.travelinsurance.repository.PolicyRepository;

@Service
public class PolicyService {

	@Autowired
	PolicyRepository policyRepository;

	public List<Policy> getAllPolicy() {
		List<Policy> policies = new ArrayList<>();
		policyRepository.findAll().forEach(policy -> policies.add(policy));
		return policies;
	}

	public Policy getPolicyById(int id) {
		return policyRepository.findById(id).get();
	}

	public void saveOrUpdate(Policy policy) {
		policyRepository.save(policy);
	}

	public void delete(int id) {
		policyRepository.deleteById(id);
	}
}
