package com.apd.travelinsurance.repository;

import org.springframework.data.repository.CrudRepository;

import com.apd.travelinsurance.entity.Customer;

public interface CustomerRepository  extends CrudRepository<Customer, Integer> {
	
	Customer findByEmail(String email);
}
