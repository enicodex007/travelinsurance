package com.apd.travelinsurance.repository;

import org.springframework.data.repository.CrudRepository;

import com.apd.travelinsurance.entity.Policy;

public interface PolicyRepository  extends CrudRepository<Policy, Integer> {
	
}
