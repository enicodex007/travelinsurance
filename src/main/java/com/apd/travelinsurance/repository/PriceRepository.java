package com.apd.travelinsurance.repository;

import org.springframework.data.repository.CrudRepository;

import com.apd.travelinsurance.entity.Price;

public interface PriceRepository  extends CrudRepository<Price, Integer> {
	
}
