package com.apd.travelinsurance.constant;

import java.util.HashMap;
import java.util.Map;

public enum Area {

	AREA_1(1),
	AREA_2(2),
	AREA_3(3),
	AREA_4(4);
	
	private final int value;
	private static Map<Integer, Area> map = new HashMap<>();
	
	private  Area(int value){
		this.value = value;
	}
	
	static {
		for (Area area : Area.values()) {
			map.put(area.value, area);
		}
	}
	
	public static Area valueOf(int area) {
		return (Area) map.get(area);
	}
	
	public int getValue() {
		return value;
	}
}
