package com.apd.travelinsurance.constant;


public enum Plan {

	
	PLAN_A("A"),
	PLAN_B("B");
	
	private final String value;
	
	private  Plan(String value){
		this.value = value;
	}
	
	public static Plan fromString(String value){
		for (Plan plan : Plan.values()) {
			if(plan.value.equalsIgnoreCase("value")) {
				return plan;
			}
		}
		return null;
	}
	
	
	public String getValue() {
		return value;
	}
}
