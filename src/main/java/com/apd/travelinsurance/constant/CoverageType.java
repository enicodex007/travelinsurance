package com.apd.travelinsurance.constant;

import java.util.HashMap;
import java.util.Map;

public enum CoverageType {

	SINGLE(1),
	ANNUAL(2);
	
	private final int value;
	private static Map<Integer, CoverageType> map = new HashMap<>();
	
	private  CoverageType(int value){
		this.value = value;
	}
	
	static {
		for (CoverageType coverageType : CoverageType.values()) {
			map.put(coverageType.value, coverageType);
		}
	}
	
	public static CoverageType valueOf(int coverageType) {
		return (CoverageType) map.get(coverageType);
	}
	
	public int getValue() {
		return value;
	}
}
