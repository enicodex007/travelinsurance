package com.apd.travelinsurance.controller;


import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.apd.travelinsurance.entity.Customer;
import com.apd.travelinsurance.entity.Policy;
import com.apd.travelinsurance.entity.Price;
import com.apd.travelinsurance.model.CustomerForm;
import com.apd.travelinsurance.model.Journey;
import com.apd.travelinsurance.model.PlanForm;
import com.apd.travelinsurance.model.SummaryForm;
import com.apd.travelinsurance.service.CustomerService;
import com.apd.travelinsurance.service.PolicyService;
import com.apd.travelinsurance.service.PriceService;

@Controller
@SessionAttributes("journey")
public class JourneyController {

	private static final Logger logger = LoggerFactory.getLogger(JourneyController.class);

	@Autowired
	private PolicyService policyService;
	@Autowired
	private PriceService priceService;
	@Autowired
	private CustomerService customerService;
	

	@RequestMapping(value = {"/","journey"}, method = RequestMethod.GET)
	public String showJourney(ModelMap model) {
		//model.put("name", getLoggedinUserName());
		model.addAttribute("journey", new Journey());
		return "journey";
	}

	@PostMapping("/saveJourney")
	public String saveJourney(@Valid @ModelAttribute("journey") Journey journey, BindingResult bindingResult, SessionStatus status, ModelMap model) {
		logger.info("saveJourney() -> journey, {}",journey.toString());

		if (bindingResult.hasErrors()) {
			return "journey";
		}
		
		Policy policy = new Policy();
		policy.setCoverageType(journey.getCoverageType().getValue());
		policy.setArea(journey.getArea().getValue());
		policy.setStartDate(journey.getStartDate());
		policy.setEndDate(journey.getEndDate());

		policyService.saveOrUpdate(policy);
		PlanForm planForm = new PlanForm();
		planForm.setPolicyId(policy.getId());
		planForm.setPlanPrices(findPrice(priceService.getAllPrice(), policy.getCoverageType(), policy.getArea()));
		
		model.addAttribute("journey", journey);
		model.addAttribute("planForm", planForm);
		return "plan";
	}
	
	

	@PostMapping("/savePlan")
	public String savePlan(@ModelAttribute("journey") Journey journey, @Valid PlanForm planForm, BindingResult bindingResult, SessionStatus status, ModelMap model) {
		logger.info("savePlan() -> plan, {}",planForm.toString());
		logger.info("savePlan() -> journey, {}",journey.toString());
		
		
		Policy policy = policyService.getPolicyById(planForm.getPolicyId());
		policy.setPlan(planForm.getPlan());
		policyService.saveOrUpdate(policy);
		
		CustomerForm customer = new CustomerForm();
		customer.setPolicyId(policy.getId());
		customer.setPlan(planForm.getPlan());
		
		model.addAttribute("journey", journey);
		model.addAttribute("customer", customer);
		return "customer";
	}

	@PostMapping("/saveCustomer")
	public String saveCustomer(@ModelAttribute("journey") Journey journey, @Valid CustomerForm customerForm, BindingResult bindingResult, SessionStatus status, ModelMap model) {
		logger.info("saveCustomer() -> customerForm, {}",customerForm.toString());
		logger.info("saveCustomer() -> journey, {}",journey.toString());
		
		/*
		if (bindingResult.hasErrors()) {
			logger.error("customerForm invalid!");
			bindingResult.getAllErrors().forEach(e -> logger.error("e->{}",e.toString()));
			return "customer";
		}*/
		
		Customer customer = new Customer();
		customer.setName(customerForm.getName());
		customer.setNric(customerForm.getNric());
		customer.setDob(customerForm.getDob());
		customer.setEmail(customerForm.getEmail());
		customer.setGender(customerForm.getGender());
		customer.setMobileNo(customerForm.getMobileNo());
		customer.setAddress1(customerForm.getAddress1());
		customer.setAddress2(customerForm.getAddress2());
		customer.setPostcode(customerForm.getPostcode());
		
		Customer existingCustomer = customerService.findByEmail(customerForm.getEmail());
		if(existingCustomer!=null) {
			logger.info("existing customer->{}", existingCustomer.getId());
			//should be performing update customer profile only.
			customer.setId(existingCustomer.getId());
		}
		customerService.saveOrUpdate(customer);
		
		Policy policy = policyService.getPolicyById(customerForm.getPolicyId());
		policy.setCustomerId(customer.getId());
		
		policyService.saveOrUpdate(policy);
		
		status.setComplete();
		
		SummaryForm summary = new SummaryForm();
		summary.setJourney(journey);
		summary.setCustomer(customerForm);
		
		model.addAttribute("summary", summary);
		return "summary";
	}
	
	private static List<Price> findPrice(List<Price> prices, int coverageType, int area) {
		logger.info("prices->{}, coverageType-{}, area-{}", prices.size(), coverageType, area);
		return prices.stream()
				.filter(p->p.getCoverage()==coverageType && p.getArea()==area)
				.collect(Collectors.toList());
	}

}
