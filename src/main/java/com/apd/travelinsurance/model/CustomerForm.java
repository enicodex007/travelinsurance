package com.apd.travelinsurance.model;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper=true)
public class CustomerForm {
	
	private int policyId;
	private String plan;
	
	@NotNull(message = "name can not be null!!")
	@NotEmpty(message = "name can not be empty!!")
	private String name;
	@NotNull(message = "nric can not be null!!")
	@NotEmpty(message = "nric can not be empty!!")
	private String nric;
	
	@NotNull(message = "dob can not be null!!")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dob; 
	
	@NotEmpty(message = "dob can not be empty!!")
	private String gender;
	
	@NotNull(message = "email can not be null!!")
	@NotEmpty(message = "email can not be empty!!")
	@Email
	private String email;
	private String mobileNo;
	private String address1;
	private String address2;
	private String postcode;

}
