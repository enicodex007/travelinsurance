package com.apd.travelinsurance.model;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.apd.travelinsurance.constant.Area;
import com.apd.travelinsurance.constant.CoverageType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper=true)
public class Journey {

	@NotNull(message = "Coverage type can not be null!!")
	private CoverageType coverageType;
	@NotNull(message = "Area can not be null!!")
	private Area area;
	@NotNull(message = "startDate can not be null!!")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date startDate;
	
	@NotNull(message = "endDate can not be null!!")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date endDate;

	//private Plan plan;
	//private Customer customer;

}
