package com.apd.travelinsurance.model;


import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.apd.travelinsurance.entity.Price;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper=true)
public class PlanForm {

	
	private int policyId;
	
	@NotNull(message = "Plan can not be null!!")
	@NotEmpty(message = "Plan can not be empty!!")
	private String plan;
	
	private List<Price> planPrices;
	
}
