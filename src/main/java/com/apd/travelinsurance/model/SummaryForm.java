package com.apd.travelinsurance.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper=true)
public class SummaryForm {
	
	private CustomerForm customer;
	private String plan;
	private Journey journey;

}
