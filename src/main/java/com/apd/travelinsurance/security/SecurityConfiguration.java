package com.apd.travelinsurance.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	/**
	@Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.inMemoryAuthentication().withUser("admin").password("admin")
                .roles("USER", "ADMIN");
    }*/
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        	.cors().disable()
        	.csrf().disable()
        	.httpBasic().disable()
        	.authorizeRequests().antMatchers("/").permitAll();
        
        http.headers().frameOptions().disable();
                //.antMatchers("/", "/*todo*/**").access("hasRole('USER')").and()
                //.formLogin();
                
    }
}
