package com.apd.travelinsurance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.apd.travelinsurance")
public class TravelInsuranceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelInsuranceApplication.class, args);
	}

}
